%% Prácticas de Matlab
%% Bucles simples
%% Hoja 2
% *Nombre:* Rodrigo
% 
% *Apellido:* Laseca Ruiz
% 
% *DNI:* 04639080A
%% Sucesiones escalares
% % 
% Consideramos las sucesiones
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} + h x_{n}^{2}\\  t_{n+1}&=& t_{n} 
% + h   \end{array}$$
% Práctica 1 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Usad índices para realizar 
% el bucle. Datos $x(1)=1$  $t(1)=1$  $h=0.1$ $N=10$ y $N=100$
% 
% *Solución:*
N1 = 9;
N2 = 99;
x11(1) = 1;
t11(1) = 1;
x12(1) = 1;
t12(1) = 1;
h = 0.1;
for i = 1:N1
    x11(i+1) = x11(i) + h*x11(i)^2;
    t11(i+1) = t11(i) + h;
end
figure(1)
plot(x11,t11,'r-*')
for i = 1:N2
    x12(i+1) = x12(i) + h*x12(i)^2;
    t12(i+1) = t12(i) + h;
end
figure(1)
plot(x12,t12,'r-*')

% Práctica 2 (Script: Bucle sin usar índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1$ 
% $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*
suc_x = @(x) x + h*x^2;
suc_t = @(t) t + h;
x21 = [];
t21 = [];
x22 = [];
t22 = [];
x21(1) = 1;
t21(1) = 1;
x22(1) = 1;
t22(1) = 1;
for i = 1:N1
    x21 = [x21,suc_x(x21(end))];
    t21 = [t21,suc_t(t21(end))];
end
figure(3)
plot(x21,t21,'r-*')
for i = 1:N2
    x22 = [x22,suc_x(x22(end))];
    t22 = [t22,suc_t(t22(end))];
end
figure(4)
plot(x22,t22,'r-*')

%% Sucesiones de varias componentes
% Consideramos las sucesiones 
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} - h y_{n}\\  y_{n+1}&=& y_{n} + h 
% x_{n}\\  t_{n+1}&=& t_{n} + h   \end{array}$$
% Práctica 3 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1)=1,$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*
ec3_x = @(x,y) x - h*y;
ec3_y = @(y,x) y + h*x;
x31 = [];
y31 = [];
x32 = [];
y32 = [];
x31(1) = 1;
y31(1) = 1;
x32(1) = 1;
y32(1) = 1;
while length(x31) < 11
      x31 = [x31,ec3_x(x31(end),y31(end))];
      y31 = [y31,ec3_y(x31(end),y31(end))];
end
figure(5)
plot(x31,y31,'r-*')
while length(x32) < 101
      x32 = [x32,ec3_x(x32(end),y32(end))];
      y32 = [y32,ec3_y(x32(end),y32(end))];
end
figure(6)
plot(x32,y32,'r-*')




% Práctica 4 (Script: Bucle sin usar índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1)=1,$ $t(1)=1,$ $h=0.1$,$ $N=10$ y $N=100$
% 
% *Solucion:*


%% Forma vectorial de las sucesiones
% Dadas las sucesiones: 
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} - h y_{n}\\  y_{n+1}&=& y_{n} + h 
% x_{n}\\  t_{n+1}&=& t_{n} + h   \end{array}$$
% 
% Escribid las dos primeras sucesiones en forma vectorial (en un papel o en 
% el propio mxl (usando el editor de ecuaciones).
% 
% *Solución:*
% Práctica 5 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script)_ abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Usad índices para realizar 
% el bucle. Datos $x(1)=1,$ $y(1)=1,$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*


% Práctica 6 (Script: Bucle sin usar índices)
% Escribid la instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1),$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*


%% Método de Euler
% Consideramos el método de Euler para el PVI:
% 
% $$  \begin{array}{cc}    \frac{dy}{dt}&=f(t,y)\\    y(t_0)&=\alpha  \end{array}$$
% 
% % 
% es decir
% 
% $$  y_{n+1}=y_n + h f(t_n,y_n).$$
% 
% Consideramos la ecuación diferencial (PVI)
% 
% $$  \begin{array}{cc}  \frac{d^2x}{dt^2}&=-x\\   x(0)&=1\\   \frac{dx(0)}{dt}&=1   
% \end{array}$$
% 
% Reescribid dicha ecuación como un sistema de ecuaciones y aplicad el método 
% de Euler. Escribid un _script _usando vuestros scripts anteriores (mejor implementar 
% sin índices) para resolver dicha EDO mediante el método de Euler. Pintad una 
% componente de la solución frente la otra.
% 
% Datos $x(1)=1,$ $\frac{dx(1)}{dt}=1,$ $t(1)=1$, $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*
y0 = [1;1];
t0 = 1;
g = @(t,y) [y(2);-y(1)];
N = [10,100];
h = 0.1;
valores_final = [];
for n = N
    valores = y0;
    t = t0;
    while length(valores) <= n-1
        valoresi = valores(:,end);
        valores = [valores,valoresi+h*g(t(end),valoresi)];
        t = [t,t(end) + h];
    end
    valores_final = [valores_final,valores];
end
figure('Name','N=10')
plot(valores_final(1,1:10),valores_final(2,1:10),'r-+')
figure('Name','N=100')
plot(valores_final(1,11:100),valores_final(2,11:100),'r-+')


%% 
% |*Ultimo valor:*|
% 
% |*x=(-0.5603,   -2.2574)*|
% 
% |*Gráfica *|
% 
%