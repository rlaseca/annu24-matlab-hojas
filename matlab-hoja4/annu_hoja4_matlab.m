%% Prácticas de Matlab
%% Diagrama de eficiencia con métodos monopaso explícitos
%% Hoja 4
% *Nombre:* Rodrigo
% 
% *Apellido:* Laseca Ruiz
% 
% *DNI:* 04639080A
% 
% *Email:* rlaseca@ucm.es
%% 
% %% 1. Diagrama de eficiencia
% Práctica 1 (El método de Euler explícito) 
% Consideramos el siguiente problema lineal
% 
% $$   y^{\prime}(t)=Ay(t)+B(t) \quad\mbox{para} \quad 0\leq t\leq 10,\quad  
% y(0)=(2,3)^{T},$$
% 
% $$    A=\left(\begin{array}{cc}        -2 & 1\\        1 & -2      \end{array}\right)    
% \qquad    B(t) =\left(\begin{array}{l}        2\sin(t)\\        2(\cos(t)-\sin(t)      
% \end{array}\right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}\left(\begin{array}{l}      1\\      1    \end{array}\right)  
% +  \left(\begin{array}{l}      \sin(t)\\      \cos(t)    \end{array}\right)$$
% 
% Se pide lo siguiente
%% 
% # Resuelve este sistema mediante el método de _Euler explícito,_ almacena 
% el máximo en valor absoluto de la diferencia entre la solución exacta y la solución 
% numérica calculada.  *Indicación:* piensa qué norma vas a usar, dependiendo 
% del tipo de salida (vector columna o vector fila) que haya producido tu algoritmo. 
% Efectúa este cálculo para varias elecciones
% # del paso $h_j$ con $j=0,\ldots,7$ siendo $h_0=0.1$, $h_j=\frac{h_0}{2^j}$. 
% Almacena los diferentes valores de $h_i$ en un vector $h_{vect}$.
% # del número de puntos $N$ siendo $N_0=100$, $N_i=2^{i}N_0$. Almacena los 
% diferentes valores de $N_i$ en un vector $N_{vect}$.
% # número de las evaluaciones totales $Ev_i$ que realiza cada algoritmo para 
% cada valor de $h_i$. Almacena los valores en un vector $Ev_{vect}$.
% # Almacena los distintos errores en un vector de nombre  *error_euler*
%% 
% Además
%% 
% * Dibuja, en una misma ventana, en escala logarítmica, el error almacenado 
% en el apartado anterior frente al paso $h$,  $h_{vect}$ *Indicación:* usa el 
% comando |loglog| en vez del comando |plot|. No use los comandos hold on, hold 
% off
% * Repite en otra figura lo mismo pero dibujando el error frente al vector  
% $N_{vect}$ 
% * Calcula la pendiente da la recta.
% * Repite en otra figura lo mismo pero dibujando el error frente al vector  
% $Ev_{vect}$.
% * Interpreta el resultado.
%% 
% %  Práctica 2 (Euler mejorado) 
% Repite el apartado anterior con el método de Euler mejorado 
% 
% % Práctica 3 (Euler modificado)
% Repite el apartado anterior con el método de Euler modificado
% 
% % Práctica 4 (Runge-Kutta 4)
% Repite el apartado anterior con el método de Runge-Kutta de orden 4.
% 
% % 
% % 
% *OJO:*  pon siempre el diagrama de eficiencia de Euler, Euler modificado, 
% Euler mejorado y Runge Kutta 4 en una gráfica como por ejemplo:
% 
% % 
% % 
% % 
% *Solución:*
format long
A = [-2 1;1 -2];
B = @(t) [2*sin(t);2*(cos(t)-sin(t))];
f = @(t,y) A*[y(1);y(2)] + B(t);
intv=[0,10];
y0=[2;3];
sol = @(t) 2*(exp(-t))*[1;1]+[sin(t);cos(t)];
h0 = 0.1;
N0 = 100;
h_vect = [];
N_vect = [];
while length(h_vect) < 7
    h_vect=[h_vect,h0/(2^(length(h_vect)+1))];
    N_vect = [N_vect,N0*(2^(length(N_vect)+1))];  
end
Ev_vect = N_vect;
error_vect = [];
for h = h_vect
    N = (intv(2)-intv(1))/h;
    [t,y]=mieuler(f,intv,y0,N);
    sols = [];
    y_norm = [];
    for ti = t
        sols = [sols,norm(sol(norm(ti)))];
    end
    for yi = y
        y_norm = [y_norm,norm(yi)];
    end
    errores = abs(y_norm-sols);
    error_max = max(errores);
    error_vect = [error_vect,error_max];
end
Ev_vect_mod = 2*N_vect;
error_vect_mod = [];
for h = h_vect
    N = (intv(2)-intv(1))/h;
    [t_mod,y_mod]=mieulermod(f,intv,y0,N);
    sols = [];
    y_norm = [];
    for ti = t_mod
        sols = [sols,norm(sol(norm(ti)))];
    end
    for yi = y_mod
        y_norm = [y_norm,norm(yi)];
    end
    errores = abs(y_norm-sols);
    error_max = max(errores);
    error_vect_mod = [error_vect_mod,error_max];
end
Ev_vect_mej = 3*N_vect;
error_vect_mej = [];
for h = h_vect
    N = (intv(2)-intv(1))/h;
    [t,y]=mieulermej(f,intv,y0,N);
    sols = [];
    y_norm = [];
    for ti = t
        sols = [sols,norm(sol(norm(ti)))];
    end
    for yi = y
        y_norm = [y_norm,norm(yi)];
    end
    errores = abs(y_norm-sols);
    error_max = max(errores);
    error_vect_mej = [error_vect_mej,error_max];
end
Ev_vect_rk4 = 4*N_vect;
error_vect_rk4 = [];
for h = h_vect
    N = (intv(2)-intv(1))/h;
    [t,y]=mirk4(f,intv,y0,N);
    sols = [];
    y_norm = [];
    for ti = t
        sols = [sols,norm(sol(norm(ti)))];
    end
    for yi = y
        y_norm = [y_norm,norm(yi)];
    end
    errores = abs(y_norm-sols);
    error_max = max(errores);
    error_vect_rk4 = [error_vect_rk4,error_max];
end
figure('Name','Gráfica de h_vect')
loglog(h_vect,error_vect,'r-+',h_vect,error_vect_mod,'b-*',h_vect,error_vect_mej,'g-o',h_vect,error_vect_rk4,'c-square')
figure('Name','Gráfica de N_vect')
loglog(N_vect,error_vect,'r-+',N_vect,error_vect_mod,'b-*',N_vect,error_vect_mej,'g-o',N_vect,error_vect_rk4,'c-square')
figure('Name','Gráfica de Ev_vect')
loglog(Ev_vect,error_vect,'r-+',Ev_vect_mod,error_vect_mod,'b-*',Ev_vect_mej,error_vect_mej,'g-o',Ev_vect_rk4,error_vect_rk4,'c-square')
%% Apéndice código: funciones de Euler, Euler modificado, Euler mejorado y Runge-Kutta 4, para calcular y pintar el diagrama de eficiencia y el orden

function [t,y]=mieuler(f,intv,y0,N)
t0 = intv(1);
h = (intv(2)-intv(1))/N;
t = t0;
y = y0;
while length(t) < N
    yi = y(:,end);
    y = [y,yi+h*f(t(end),yi)];
    t = [t,t(end) + h];
end
end

function [t,y]=mieulermod(f,intv,y0,N)
t0 = intv(1);
h = (intv(2)-intv(1))/N;
t = t0;
y = y0;
while length(t) < N
    yi = y(:,end);
    y = [y,yi+h*f(t(end)+h/2,yi+(h/2)*f(t(end),yi))];
    t = [t,t(end) + h];
end
end

%Método de Euler mejorado
function [t,y]=mieulermej(f,intv,y0,N)
t0 = intv(1);
h = (intv(2)-intv(1))/N;
t = t0;
y = y0;
while length(t) < N
    yi = y(:,end);
    y = [y,yi+(h/2)*(f(t(end),yi)+f(t(end)+h,yi+h*f(t(end),yi)))];
    t = [t,t(end) + h];
end
end

function [t,y]=mirk4(f,intv,y0,N)
t0 = intv(1);
h = (intv(2)-intv(1))/N;
t = t0;
y = y0;
while length(t) < N
    yi = y(:,end);
    ti = t(end);
    F1 = f(ti,yi);
    F2 = f(ti+(h/2),yi+h*F1/2);
    F3 = f(ti+(h/2),yi+h*F2/2);
    F4 = f(ti + h,yi + h*F3);
    y = [y,yi+(h/6)*(F1+2*F2+2*F3+F4)];
    t = [t,t(end) + h];
end
end
